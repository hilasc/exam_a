import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule,Routes} from '@angular/router';
import{AngularFireModule} from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesService } from './invoices/invoices.service';
import { InvoiceComponent } from './invoice/invoice.component';

export const firebaseConfig = {
    apiKey: "AIzaSyA19oJtWIBDMb31Ld0VNeBrJJ4s-Sr1bLM",
    authDomain: "exam-5c7c0.firebaseapp.com",
    databaseURL: "https://exam-5c7c0.firebaseio.com",
    storageBucket: "exam-5c7c0.appspot.com",
    messagingSenderId: "1089478704434"
}

const appRoutes:Routes = [
  //{path:'users', component:UsersComponent},
  //{path:'posts', component:PostsComponent},
  {path:'invoices', component:InvoicesComponent},
  {path:'invoiceForm', component:InvoiceFormComponent},
  {path:'', component:InvoiceFormComponent},
  {path:'**', component:PageNotFoundComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    InvoiceFormComponent,
    InvoicesComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)

  ],
  providers: [InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
